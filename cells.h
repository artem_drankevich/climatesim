#ifndef CELLS_H
#define CELLS_H

#include <QVector>
#include "coordssystem.h"


class Cells
{
public:
    Cells();
    Cells(const CoordsSystem sys);

    void set(const CoordsSystem sys);

    QVector<double> data(const int id);
    QVector<double> &rdata(const int id);

    double &rtemp(const int i);
    double temp(const int i);
    double &rrad(const int i);
    double rad(const int i);
    double &rhei(const int i);
    double hei(const int i);
    double &rpres(const int i);
    double pres(const int i);
    double &rhum(const int i);
    double hum(const int i);


    QList<double> wind_angle;
    QList<QVariant> wind_color;

    QList<QString> info_data;
    QList<QVariant> color_data;

private:
    QVector<double> temperatures;
    QVector<double> radiation;
    QVector<double> heights;
    QVector<double> pressures;
    QVector<double> humids;

    CoordsSystem system;
};

#endif // CELLS_H

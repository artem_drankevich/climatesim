#include "point.h"
#include "control.h"

Point::Point()
{
    Neibours.fill(nullptr, 4);
}

/* EDIT
 *  - fixed cloud cover reflect coeff
 *  - sum of 2 reflection koeffs
 *  - sun angle display
 *  - curents
 *  - rain fix
 *  - avgs
 *  - cyclone effect include dPresuure
 */

void Point::solarRadiadion(const double sunAngle)
{
    // test
    Status_text = "";
    Falls = 0;
    //Pressure = -cos(Pos.y() * 6) * 0.6;
    // count max vapour
    maxHumidity = count_vapourMass(Temp_air - fmax(0, (Height - 60) * 0.1)) * 10; // height coeff

    //get temperature (equation)
    double absCloudK = Cloudness / maxCloudness;
    if (absCloudK != absCloudK || absCloudK > 1 || absCloudK < 0)
        absCloudK = 1;
    double absorbtion= 1 - (Humidity * Humidity / maxHumidity/ maxHumidity * absCloudK);
    if(Temp_ground < -2) { // snow
        absorbtion *= 0.2; // 0.03 ABSORBTION??
    }
    if (Height > 60) { // ocean/land effect
        absorbtion *= 5;
    }
    if(sunAngle > 0) // solar rad MUL BY 5
        Temp_ground += 5*0.090 * pow(sin(Control::toRad(sunAngle)), 2) * absorbtion; // 0.085
    Temp_ground -= 5*0.012 * pow(Temp_ground / 273 + 1, 4) * absorbtion ;// power must be 4!
    //Temp_ground = cos(Control::toRad(Pos.y())) * 30 - 15;
    // temp exchange between air and ground
    Temp_air +=(Temp_ground - Temp_air) * 0.12;
    Temp_ground +=(Temp_air - Temp_ground) * 0.005;

    // count max vapour
    maxHumidity = count_vapourMass(Temp_air - fmax(0, (Height - 60) * 0.1)) * 10; // height coeff
    //====humidity=============================================================
    //if(Height <= 60) { // sea tile (temp solution)
    // ENABLE GROUND FALLS --> HUMIDITY
    double dH = 1013/(Pressure + 1013) * (maxHumidity - Humidity) * (273 + pow(1.2/*1.15*/, Temp_ground)) * (1 + 0.134 * pow(0.0001 + Wind.length(), 1.3)) * 0.00002;
    if(Temp_ground < 0) // frozen
        dH *= 0.05;

    if (dH < 0) // prevent errors
        dH = 0;
    if (Height <= 60) {
        Humidity += dH;
    } else {
        if (dH > Falls_Holded)
            dH = Falls_Holded;
        Falls_Holded -= dH;
    }
    //}

    if (Humidity > maxHumidity) // fog (disabled?)
    {
        Falls += (Humidity - maxHumidity) * 0.1;
        Humidity = maxHumidity;
        Status_text+= "\nfog";
    }

}
void Point::updateWind()
{
    //==========count wind x and y speed
    double dx = -Neibours[0]->pressure() + Neibours[2]->pressure();
    double dy = -Neibours[1]->pressure() + Neibours[3]->pressure();
    //qInfo() << "dx " << dx << " dy " << dy;
    // count temperatute wind?
    double tdx = 0.01 * ( -Neibours[2]->temp_air() + Neibours[0]->temp_air());
    double tdy = 0.01 * ( -Neibours[3]->temp_air() + Neibours[1]->temp_air());
    Vector t_wind(tdx,tdy);
    //qInfo() << "tdx " << tdx << " tdy " << tdy;

    dx = sgn(dx) * sqrt(abs(dx));
    dy = sgn(dy) * sqrt(abs(dy));
    //qInfo() << "dx " << dx << " dy " << dy;

    //qInfo() << "!!x " << Wind.x() << " y " << Wind.y();

    Wind =  Vector(dx, dy);
    //Wind += t_wind;

    //Coryolis Effect=============================
    Vector coryolis = Wind.normal_anticlockwise();
    coryolis.multiply(3*sin(Control::toRad(Pos.y()))); // !!!Consts!!!

    //qInfo() << "x " << Wind.x() << " y " << Wind.y();

    Wind += coryolis;

    // friction
    if (Height > 60)
        Wind.multiply(0.95);

    //wind obstacle reflection force
    double friction_x = (- fmax(60, Neibours[0]->height()) + fmax(60, Neibours[2]->height())); // 60 is sea level, so its temporal solution
    double friction_y = (- fmax(60, Neibours[1]->height()) + fmax(60, Neibours[3]->height()));

    Vector friction_force = Vector(sgn(friction_x) * pow(fmin(abs(friction_x), 90) / 90, 2),
                                   sgn(friction_y) * pow(fmin(abs(friction_y), 90) / 90, 2));

    friction_force.multiply(Wind.length() * 0.5);

    //qInfo() << "x " << Wind.x() << " y " << Wind.y();

    Wind += friction_force;
}
void Point::diffuse(const double tile_const)
{
    dPressure = diffusion(Pressure, Neibours[0]->pressure(), Neibours[1]->pressure(), Neibours[2]->pressure(), Neibours[3]->pressure(), tile_const /** 0.5*/);
    dTemp_air = diffusion(Temp_air, Neibours[0]->temp_air(), Neibours[1]->temp_air(), Neibours[2]->temp_air(), Neibours[3]->temp_air(), tile_const /** 0.5*/);
    dHumidity = diffusion(Humidity, Neibours[0]->humidity(), Neibours[1]->humidity(), Neibours[2]->humidity(), Neibours[3]->humidity(), tile_const /** 0.5*/);
    dCloudness = diffusion(Cloudness, Neibours[0]->cloudness(), Neibours[1]->cloudness(), Neibours[2]->cloudness(), Neibours[3]->cloudness(), tile_const /** 0.5*/);

    if (Neibours[0]->Height <= 60)
        dTemp_ground = diffusion(Temp_ground,
                Neibours[0]->Height <= 60 ? Neibours[0]->temp_ground() : Temp_ground,
                Neibours[1]->Height <= 60 ? Neibours[1]->temp_ground() : Temp_ground,
                Neibours[2]->Height <= 60 ? Neibours[2]->temp_ground() : Temp_ground,
                Neibours[3]->Height <= 60 ? Neibours[3]->temp_ground() : Temp_ground,
                tile_const /** 0.5*/);
    else
        dTemp_ground = 0;
}
void Point::advect(Point &p, const double koeff)
{
    advection_point ap = {&p, koeff}; //humidity, temp_air, cloudness, koeff;
    advection_data.push_back(ap);
}

void Point::implement()
{
    // implement current flow
    if (Height < 60)
    {
        double nTemp_ground = 0, fPressure = 0;
        for (advection_point &ap : currents_data) {
            nTemp_ground  += ap.p->temp_ground()  * ap.koeff;
            fPressure  += ap.koeff; // nPressure is amout of air (1 -- is normal) that is recieved by point; <1 - low pressure; >1 -- high; this value ignored/affected by cyclones
        }
        if (abs(fPressure) < 0.001) {
            qWarning() << "fPressure = 0!!!!";
        } else if (Height < 60) {
            Temp_ground  = nTemp_ground / fPressure;
        }
    }
    // implement advection
    double nTemp_air = 0, nHumidity = 0, nCloudness = 0, nPressure = 0;
    nPressure = 0;
    for (advection_point &ap : advection_data) {
        nTemp_air  += ap.p->temp_air()  * ap.koeff;
        nHumidity  += ap.p->humidity()  * ap.koeff;
        nCloudness += ap.p->cloudness() * ap.koeff;
        nPressure  += ap.koeff; // nPressure is amout of air (1 -- is normal) that is recieved by point; <1 - low pressure; >1 -- high; this value ignored/affected by cyclones
    }
    if (abs(nPressure) < 0.001) {
        //qWarning() << "nPressure = 0!!!!";
    } else {
        Temp_air  = nTemp_air / nPressure;
    }
    Humidity  = nHumidity;
    Cloudness = nCloudness;
    //qInfo() << "nPressure " << nPressure;
    advection_data.clear();

    // implement diffusion
    CycloneEffect += dPressure;

    Pressure  += dPressure;
    Temp_air  += dTemp_air;
    Humidity  += dHumidity;
    Cloudness += dCloudness;

    Temp_ground += dTemp_ground;
    // count

    maxHumidity = count_vapourMass(Temp_air - fmax(0, (Height - 60) * 0.1)) * 10; // height coeff

    // cloud formation
    double koeff = -Pressure * 0.03 + (Temp_ground - Temp_air) * 0.01; // CYCLONE EFF
    //qInfo()<<"  koeff " <<koeff;
    if (koeff > 0)
    {
        Cloudness += Humidity*koeff; // koeff
        Humidity *= (1 - koeff);
    }

    //rain
    maxCloudness = maxHumidity * 0.5 * pow(1 + fmax(0, Pressure * 0.1), 1.5);// CYCLONE EFF
    if ((Cloudness > maxCloudness || (Status_text.indexOf("rain") != -1 && Cloudness > maxCloudness * 0.1)))
    {
        double dc = Control::getRandom(5, 25) * 0.01 * maxCloudness;
        Cloudness -= dc;
        Falls += dc * 0.1;// falls scale
        if (dc > 50)
            Status_text+= "\nhardrain (mm " + QString::number(dc) + ")";
        else if (dc > 10)
            Status_text+= "\nrain (mm " + QString::number(dc) + ")";
        else
            Status_text+= "\nlightrain (mm " + QString::number(dc) + ")";
        Temp_ground -= Temp_ground * 0.01;

        // CYCLONE EFF
        //int thunderIndex = (0.0134 * powf(1 + Wind.length(), 1.4) * powf(1 + fmax(0, -Pressure * 0.2), 2) * (1 + dc * 0.05) /*+ (1 + 0.1 * abs(Temp_air - Temp_ground))*/);
        //if (thunderIndex > 0)
         //   Status_text += "\nstorm " + QString::number(thunderIndex);
    }

    Falls_Holded += Falls;

    //humids[i]=round(vapour[i]/max_vapour[i]*100);//100 must
}

void Point::updateCurrents()
{

    if (Height >= 60)
    {
        Current = Vector(0, 0);
        return;
    }

    Vector current(0, cos(Control::toRad(Pos.y() * 6)));
//    Vector coryolis;
//    if (lon >= 0)
//        coryolis = current.normal_anticlockwise();
//    else
//        coryolis = current.normal_clockwise();
//    coryolis.multiply(0.3); // !!!Consts!!!
//    current += coryolis;
    // shore hit?
//    for (Point* neibour : Neibours) {
//        if (neibour->height() >= 60)

//    }
    double friction_x = (- fmin(60, Neibours[0]->height()) + fmin(60, Neibours[2]->height())); // 60 is sea level, so its temporal solution
    double friction_y = (- fmin(60, Neibours[1]->height()) + fmin(60, Neibours[3]->height()));
    Vector friction = Vector(friction_x, friction_y);
    // check if this is shore point
    bool ifShore = false;
    for (Point *p : Neibours)
        if (p->height() >= 60 /*shore*/)
        {
            ifShore = true;
            break;
        }
    if (ifShore)
        Current = friction;
    else
        Current = current;


//    Vector friction = Vector(sgn(friction_x) * pow(fmin(abs(friction_x), 90) / 90, 2),
//                                   sgn(friction_y) * pow(fmin(abs(friction_y), 90) / 90, 2));

    Current.normalize();
    Current = Current.normal_anticlockwise();
    Current.multiply(6);
//    friction = friction.normal_clockwise();
//    friction.multiply(sgn(sin(Control::toRad(6*Pos.y()))));

//    current.multiply(5);
//    friction.multiply(current.length() * 5);//koeff
//    current += friction;
//    if (Height < 60)
//        Current = current;//Vector(0, 0);//current;
//    else
//        Current = Vector(0, 0);
}

void Point::cycloneEffect(const double cyclone_effect)
{
    CycloneEffect = cyclone_effect;
    Pressure += cyclone_effect;
}

void Point::updateAvgs(const QDateTime &now)
{
    const int month = now.date().month()-1;
    average_data[month].avgC += Cloudness / maxCloudness;
    average_data[month].avgH += Humidity / maxHumidity;
    average_data[month].avgFalls += Falls;
    average_data[month].avgP += Pressure;
    average_data[month].avgT += Temp_air;

    average_data[month].foggyDays += Status_text.indexOf("fog") != -1; // hours!! not days
    average_data[month].summerDays += Temp_air > 0;
    average_data[month].rainyDays += Status_text.indexOf("rain") != -1;
    average_data[month].sunnyDays += Cloudness / maxCloudness < 0.3;

    int daysInMonth = now.date().daysInMonth();
    if (now.date().day() == daysInMonth && now.time().hour() == 23)
    {
        avg_year_temp = 0., avg_year_falls = 0.;
        double percent_sunny = 0., percent_summer = 0.;
        for (int i = 0; i < 12; i++)
        {
            avg_year_temp+= average_data[i].avgT / 365 / 24;
            avg_year_falls+= average_data[i].avgFalls;
            percent_sunny+= average_data[i].sunnyDays / 365. / 24.;
            percent_summer+= average_data[i].summerDays / 365. / 24.;
        }

        daysInMonth *= 24;

        Avgs_status_text = "\n<T>=" + QString::number(average_data[month].avgT / daysInMonth) +
                "\n<H>=" + QString::number(average_data[month].avgH / daysInMonth) +
                "\n<C>=" + QString::number(average_data[month].avgC / daysInMonth) +
                "\n<Falls>=" + QString::number(average_data[month].avgFalls / daysInMonth) +
                "\n<P>=" + QString::number(average_data[month].avgP / daysInMonth) +
                "\nsummer hours=" + QString::number(average_data[month].summerDays) +
                "\nsunny hours=" + QString::number(average_data[month].sunnyDays) +
                "\nrainy hours=" + QString::number(average_data[month].rainyDays) +
                "\nfoggy hours=" + QString::number(average_data[month].foggyDays) +
                "\n<Tyear> " + QString::number(avg_year_temp) +
                "\nySum Falls " + QString::number(avg_year_falls) +
                "\nsunnyOfYear " + QString::number(percent_sunny*100) + "%" +
                "\nsummerOfYear " + QString::number(percent_summer*100) + "%";
        // clear data for next month
        average_data[(month + 1) % 12] = {0., 0., 0., 0., 0., 0, 0, 0, 0};
    }
}

QString Point::getClimate()
{
    int index_temp = fmin(fmax(round((avg_year_temp - fmax(0, (Height - 60) * 0.1) + 12.5) / 2.5), 0), 17 ); // with elevation check
    int index_falls = fmin(avg_year_falls / 250, 9); // temp log // CONTROL!!!!
    Status_text += "\nindex_temp " + QString::number(index_temp) +  " index_falls " + QString::number(index_falls);
    //if (Height > 240)
        //return "highlands";
    if (Height < 60)
        return "sea";
    return Biomes_Names[index_temp][index_falls];
}

QString Point::getFallsType()
{
    // this variant is temporal
    if (Status_text.indexOf("storm")!= -1)
        return "storm";
    if (Status_text.indexOf("hardrain")!= -1)
        return "hardrain";
    if (Status_text.indexOf("lightrain")!= -1)
        return "lightrain";
    if (Status_text.indexOf("rain")!= -1)
        return "rain";
    if (Status_text.indexOf("fog")!= -1)
        return "fog";
    return "no";
}

bool Point::isError()
{
    return (abs(Pressure) > 1000 || abs(Temp_air) > 2000 || Temp_air != Temp_air || abs(Temp_ground) > 2000 || Temp_ground != Temp_ground || abs(Humidity)>20000 || Humidity != Humidity);
}

QString Point::formReturnInfo()
{
    return "\nHeight " + QString::number(Height) +
            "\nPressure " + QString::number(Pressure) + " dP " + QString::number(dPressure) + " CE " + QString::number(CycloneEffect) +
            "\nWind " + QString::number(Wind.length()) + "m/s " + QString::number(Wind.angle()) +
            "\nTemp_ground " + QString::number(Temp_ground) + " dT " + QString::number(dTemp_ground) +
            "\nTemp_air " + QString::number(Temp_air) + " dT " + QString::number(dTemp_air) +
            "\nHumidity " + QString::number(Humidity) + " rH " + QString::number(round(Humidity / maxHumidity * 100)) +
            "\nCloudness " + QString::number(Cloudness) + " rC " + QString::number(round(Cloudness / maxCloudness * 100)) +
            "\nFalls " + QString::number(Falls) +
            "\nFallsHold " + QString::number(Falls_Holded) +
            "\nCLIMATE " + getClimate()
            + Status_text + Avgs_status_text;
}

QVariant Point::getData()
{
    return QVariant();
}

double &Point::rsunAngle()
{
    return SunAngle;
}
double &Point::rpressure()
{
    return Pressure;
}
double &Point::rtemp_air()
{
    return Temp_air;
}
double &Point::rtemp_ground()
{
    return Temp_ground;
}
double &Point::rhumidity()
{
    return Humidity;
}
double &Point::rcloudness()
{
    return Cloudness;
}
Vector &Point::rwind()
{
    return Wind;
}
double &Point::rfalls()
{
    return Falls;
}
double &Point::rheight()
{
    return Height;
}
QVector<Point *> &Point::rneibours()
{
    return Neibours;
}
QPoint &Point::rpos()
{
    return Pos;
}

QVector<advection_point> &Point::radvection_data()
{
    return advection_data;
}
QVector<advection_point> &Point::rcurrents_data()
{
    return currents_data;
}

void Point::setNeibours(Point &p0, Point &p1, Point &p2, Point &p3)
{
    Neibours[0] = &p0;
    Neibours[1] = &p1;
    Neibours[2] = &p2;
    Neibours[3] = &p3;
}

double Point::sunAngle() const
{
    return SunAngle;
}

double Point::pressure() const
{
    return Pressure;
}
double Point::temp_air() const
{
    return Temp_air;
}
double Point::temp_ground() const
{
    return Temp_ground;
}
double Point::humidity() const
{
    return Humidity;
}

double Point::humidity_max() const
{
    return maxHumidity;
}
double Point::cloudness() const
{
    return Cloudness;
}
Vector Point::wind() const
{
    return Wind;
}

Vector Point::current() const
{
    return Current;
}
double Point::falls() const
{
    return Falls;
}
double Point::height() const
{
    return Height;
}

QVector<Point *> Point::neibours() const
{
    return Neibours;
}

QPoint Point::pos() const
{
    return Pos;
}


void Point::reset()
{
    Pressure = 0;
    Temp_air = 0;
    Temp_ground = 0;
    Humidity = 0;
    Cloudness = 0;
    maxCloudness = 0;
    Falls = 0;
    Falls_Holded = 0;
    Wind = Vector(0, 0);
    Status_text = "";
}

double Point::diffusion(const double value, const double value_0, const double value_1, const double value_2, const double value_3, const double koeff)
{
    const double px = value_0 + value_2 - 2 * value;
    const double py = value_1 + value_3 - 2 * value;
    return koeff * (px + py);
}

double Point::count_sunAngle(const QDateTime time)
{

        double timeAngle=(time.time().hour() * 60+ Pos.x()/15 *60+ time.time().minute() - 720) / 4;

        double decl = asin(
                    sin(Control::toRad(23.45)) * sin(
                        Control::toRad(360 / 365.25 * ( time.date().dayOfYear()- 81))));
        double x = cos(timeAngle * PI / 180) * cos(decl);
        double z = sin(decl);
        double zhor = x * cos(Pos.y() * PI / 180) + z * sin(Pos.y() * PI / 180);

        return (asin(zhor) * (180 / PI));
}
double Point::count_vapourMass(const double temp)
{
    return 136 * pow(2.71828, (18.3036 - 3816.44 / (temp+273-46.13))) * 29 / 8.31 / (temp+273);
}
//Point::Point(double temp,double vap,double x,double y)
//{
//    temperature=temp;
//    vapour=vap;
//    this->x=x;
//    this->y=y;
//    lifespan=240;
//}

//void Point::move(const double dx,const double dy)
//{
//    x+=dx;
//    y+=dy;
//    lifespan--;
//}

//void Point::get(double &dx,double &dy)
//{
//    dx=x;
//    dy=y;
//}

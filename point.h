#ifndef POINT_H
#define POINT_H

#include <QVector>
#include <QObject>
#include <QHash>
#include <QColor>
#include "vector.h"

class Point;

struct advection_point
{
    Point *p;
    double koeff;
};
struct average_point
{
    double avgT;
    double avgP;
    double avgH, avgC;
    double avgFalls;
    int sunnyDays, rainyDays, foggyDays, summerDays;
};

//static const QVector<QVector<QString>> Biomes_Names = {
//    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
//    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
//    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
//    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
//    { "desert", "taiga",    "taiga", "taiga", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra" },
//    { "desert", "taiga",    "taiga", "taiga", "taiga",    "tundra", "tundra", "tundra", "tundra", "tundra" },
//    { "desert", "grassland", "taiga", "taiga", "taiga",   "taiga", "tundra", "tundra", "tundra", "tundra" },
//    { "desert", "grassland", "forest", "taiga", "forest", "rainforest", "tundra", "tundra", "tundra", "tundra" },
//    { "desert", "grassland", "forest", "forest", "forest", "forest", "rainforest", "tundra", "tundra", "tundra" },
//    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "tundra", "tundra" },
//    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "tundra", "tundra" },
//    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "rainforest", "tundra" },
//    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "rainforest", "rainforest" },
//    { "desert", "thorn",    "savana", "thorn", "tropical", "tropical", "tropical", "rainforest", "rainforest", "rainforest" },
//    { "desert", "thorn",    "thorn", "thorn", "tropical",  "tropical", "tropical", "jungle", "jungle", "jungle" },
//    { "desert", "thorn",    "thorn", "thorn", "tropical",  "tropical", "tropical", "jungle", "jungle", "jungle" },
//    { "desert", "desert",   "thorn", "thorn", "tropical",  "tropical", "tropical", "tropical", "jungle", "jungle" },
//    { "desert", "desert",   "desert", "desert", "thorn",   "thorn",    "thorn", "thorn", "thorn", "thorn" }
//};
static const QVector<QVector<QString>> Biomes_Names = {
    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
    { "tundra", "tundra",   "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra", "tundra" },
    { "desert", "taiga",    "taiga", "taiga", "taiga",   "taiga", "mtaiga", "mtaiga", "mtaiga", "mtaiga" },
    { "desert", "taiga",    "taiga", "taiga", "taiga",    "taiga", "mtaiga", "mtaiga", "mtaiga", "mtaiga" },
    { "desert", "grassland", "taiga", "taiga", "taiga",   "taiga", "mtaiga", "mtaiga", "mtaiga", "mtaiga" },
    { "desert", "grassland", "forest", "forest", "forest", "rainforest", "rainforest", "swamps", "swamps", "swamps" },
    { "desert", "grassland", "forest", "forest", "forest", "forest", "rainforest", "rainforest", "swamps", "swamps" },
    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "rainforest", "swamps" },
    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "rainforest", "rainforest" },
    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "rainforest", "rainforest" },
    { "desert", "savana",   "savana", "forest", "forest", "forest", "rainforest", "rainforest", "rainforest", "rainforest" },
    { "desert", "thorn",    "savana", "thorn", "tropical", "tropical", "tropical", "rainforest", "rainforest", "rainforest" },
    { "desert", "thorn",    "thorn", "thorn", "tropical",  "tropical", "tropical", "jungle", "jungle", "jungle" },
    { "desert", "thorn",    "thorn", "thorn", "tropical",  "tropical", "tropical", "jungle", "jungle", "jungle" },
    { "desert", "desert",   "thorn", "thorn", "tropical",  "tropical", "tropical", "tropical", "jungle", "jungle" },
    { "desert", "desert",   "desert", "desert", "thorn",   "thorn",    "thorn", "thorn", "thorn", "thorn" }
};

static const QHash<QString, QColor> Biome_Colors =
{
    {"tundra", QColor("aliceblue")},
    {"grassland", QColor("lightgreen")},
    {"taiga", QColor("blueviolet")},
    {"forest", QColor("forestgreen")},
    {"rainforest", QColor("lightseagreen")},
    {"jungle", QColor("teal")},
    {"forest", QColor("forestgreen")},
    {"thorn", QColor("peru")},
    {"savana", QColor("olive")},
    {"tropical", QColor("limegreen")},
    {"desert", QColor("gold")},
    {"highlands", QColor("gray")},
    {"sea", QColor("skyblue")},
    {"mtaiga", QColor("darkslateblue")},
    {"swamps", QColor("cadetblue")}
};
static const QHash<QString, QColor> Falls_Colors =
{
    {"no", QColor("gray")},
    {"rain", QColor("dodgerblue")},
    {"lightrain", QColor("cornflowerblue")},
    {"hardrain", QColor("mediumslateblue")},
    {"storm", QColor("gold")},
    {"fog", QColor("lavender")}
};


class Point
{
public:
    Point();
//    Point(double temp,double vap,double x,double y);
//    //Point's neibors and theirs IDs:
//    // |-||3||-|
//    // |2||p||0|
//    // |-||1||-|
//    //
//    void move(const double dx, const double dy);
//    void get(double &dx, double &dy);

    void solarRadiadion(const double sunAngle);
    void updateWind();
    void diffuse(const double tile_const);
    void advect(Point& p, const double koeff);
    void implement(); // just affect point with changes done with advection and diffusion
    void updateCurrents();
    void currentFlow();

    void cycloneEffect(const double cyclone_effect);

    void updateAvgs(const QDateTime &now);

    QString getClimate();
    QString getFallsType();

    bool isError();

    QString getAvgs();
    void cntMonthAvg();
    void cntFullAvg();

    QString formReturnInfo();
    QVariant getData();

    double &rsunAngle();
    double &rpressure();
    double &rtemp_air();
    double &rtemp_ground();
    double &rhumidity();
    double &rcloudness();
    double &rfalls();
    double &rheight();
    Vector &rwind();
    QPoint &rpos();

    QVector<advection_point> &radvection_data();
    QVector<advection_point> &rcurrents_data();

    QVector<Point *> &rneibours();
    void setNeibours(Point &p0, Point &p1, Point &p2, Point &p3);

    double sunAngle() const;
    double pressure() const;
    double temp_air() const;
    double temp_ground() const;
    double humidity() const;
    double humidity_max() const;
    double cloudness() const;
    Vector wind() const;
    Vector current() const;
    double falls() const;
    double height() const;
    QVector<Point *> neibours() const;
    QPoint pos() const;


    void reset();

private:
    double SunAngle;
    double Pressure, dPressure;
    double Temp_air, dTemp_air;
    double Temp_ground, dTemp_ground;
    double Humidity, dHumidity, maxHumidity;
    double Cloudness, dCloudness, maxCloudness;
    double CycloneEffect;
    Vector Wind, Current;
    QPoint Pos;
    QString Status_text;

    double Falls, Falls_Holded;
    double Height;

    double avg_year_temp, avg_year_falls;

    QVector<Point*> Neibours;
    QVector<advection_point> advection_data; // temp solution (->list of structures)
    QVector<advection_point> currents_data; // potoki

    QVector<average_point> average_data = QVector<average_point>(12);
    QString Avgs_status_text;
//    //Point's neibors and theirs IDs:
//    // |-||3||-|
//    // |2||p||0|
//    // |-||1||-|
//    //
//    double x;
//    double y;

    double diffusion(const double value, const double value_0, const double value_1, const double value_2, const double value_3, const double koeff); // 0-3 is corresponded to point neibours pos

    double count_sunAngle(const QDateTime time);
    double count_vapourMass(const double temp);
};

//struct qml_point
//{
//    qml_point()
//    {

//    }
//    ~qml_point()
//    {

//    }
//    qml_point(Point &p)
//    {
//        Pressure = p.pressure();
//        Temp_air = p.temp_air();
//        Temp_ground = p.temp_ground();
//        Humidity = p.humidity();
//        Cloudness = p.cloudness();
//    }
//    Q_GADGET
//    double Pressure;
//    double Temp_air;
//    double Temp_ground;
//    double Humidity;
//    double Cloudness;
//    double WindSpeed, WindAngle;

//    double Falls;
//    double Height;
//};

//Q_DECLARE_METATYPE(qml_point)


#endif // POINT_H

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "control.h"

int main(int argc, char *argv[])
{

    qmlRegisterType<Control>("control", 1, 0, "Control");

    QGuiApplication a(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return a.exec();
}


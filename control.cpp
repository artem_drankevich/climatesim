#include "control.h"
#include "cyclone.h"
#include "vector.h"

//#define CYCLONES_STOP
//#define LOOP_TIME
#define DIFFUSION_ENABLE 1.0
#define CURRENTFLOW_ENABLE
#define ADVECTION_ENABLE
void advect_wind(Point &p, Point *member, const double koeff);
void advect_current(Point &p, Point *member, const double koeff);

Control::Control(QObject *parent) : QObject(parent)
{
    //img.load("test.png");
    //img.load("timed1.jpg");
    img.load("timed1.jpg");
    //img.load("test3.png");
    qInfo()<< "HEIGHTMAP IMG loaded?" << !img.isNull();

    time  = QDateTime::fromString("2000-03-01 00:00:00","yyyy-MM-dd hh:mm:ss");
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(triggered()));

    polar_system.set(-180, -90, 180, 90);

    // set cyclones (itll be moved into separate method)
    cyclones.fill(Cyclone(polar_system), cyclones_max);

    clearAll();
}

Control::~Control()
{

}

void Control::triggered() {

    time = time.addSecs(timespep);

#ifdef LOOP_TIME
    time.setDate(QDate::fromString("2000-03-22", "yyyy-MM-dd"));
#endif

    tick();

    emit Control::timeChanged();
}

void Control::toggleTime()
{
    if(timer->isActive())
        timer->stop();
    else
        timer->start(0);//!!!
}
void Control::discharge()
{
    qInfo()<<"discharge";
    time = QDateTime::fromString("2000-03-01 00:00:00","yyyy-MM-dd hh:mm:ss");
    clearAll();
}
void Control::clearAll()
{
    for (Point &p : points)
        p.reset();
}
void Control::setValue(int i, double val, int type){
    switch (type) {
    case 0:
        if (points[i].height()+val >= 0)
        {
            points[i].rheight()+=val;

            points[i].updateCurrents();
            for (auto &p : points[i].neibours())
                p->updateCurrents();
        }
        break;
    case 1:
        points[i].rtemp_air() += val;
        break;
    case 3:
        points[i].rpressure() +=val;
        break;
    case 4:
        points[i].rhumidity()+=val;
        break;
    case 6:
        points[i].rtemp_ground()+=val;
        break;
    default:
        qInfo()<<"Problem! This data type isnt supported!";
    }
}

void Control::tick()
{

    //qInfo()<<"=============DEBUG============================";
    QString res = "";

    //update all cyclones
#ifndef CYCLONES_STOP
    for (Cyclone &c : cyclones)
    {
        bool isAlive;
        QPointF cyclone_loc = c.update(sum_pressure, isAlive);
        if (!isAlive)
            continue;

        //get cyclone affect on cells
        QPointF cyclone_tile = points_system.translateFrom(polar_system, cyclone_loc);

        points[points_system.toLinear(int(cyclone_tile.x())    ,int(cyclone_tile.y()))    ].
                cycloneEffect(c.getForce());
        points[points_system.toLinear(int(cyclone_tile.x()) + 1,int(cyclone_tile.y()) + 1)].
                cycloneEffect(c.getForce());
        points[points_system.toLinear(int(cyclone_tile.x()) + 1,int(cyclone_tile.y()))    ].
                cycloneEffect(c.getForce());
        points[points_system.toLinear(int(cyclone_tile.x())    ,int(cyclone_tile.y()) + 1)].
                cycloneEffect(c.getForce());

    }
#endif


    int tile_x, tile_y;
    int i = 0;

    const double tile_const =  3.6 / km_in_tile;
    for (Point &p : points)
    {
        //get cur tile x y
        points_system.toBilinear(i, tile_x, tile_y);

        //get geo cords
        QPointF geo_coords = polar_system.translateFrom(points_system, QPointF(tile_x, double(tile_y) + 0.5));
        geo_coords.ry()*= -1;

        //get sun angle and ret it
        double sunAngle = count_sunAngle(geo_coords.y(), geo_coords.x()); // radiation
        p.rsunAngle() = sunAngle;
        p.solarRadiadion(sunAngle);


        //
        p.updateWind();

        i++;
    }
    i = 0;
    //spread values
    for (Point &p : points)
    {
#ifdef DIFFUSION_ENABLE
        p.diffuse(tile_const * DIFFUSION_ENABLE);
#endif
#ifdef CURRENTFLOW_ENABLE
        advection(p, i, p.current(), tile_const, advect_current);
#endif
#ifdef ADVECTION_ENABLE
        advection(p, i, p.wind(), tile_const, advect_wind);
#endif
        i++;
    }

    sum_pressure = 0;
    //temp
    info_data.clear();

    i = 0;
    //affect cells
    for (Point &p : points)
    {
        //get cur point x and y
        points_system.toBilinear(i,tile_x,tile_y);

        //get geo cords
        QPointF geo_coords = polar_system.translateFrom(points_system,QPointF(tile_x,double(tile_y) + 0.5));
        geo_coords.ry()*=-1;

        //get GMT
        int gmt = int(floor((geo_coords.x() + 7.5) / 15));

        //affect cells
        p.implement();
        sum_pressure += p.pressure();

        info_data.push_back("cell lat "+QString::number(int(geo_coords.y()))+" lon "+QString::number(int(geo_coords.x())) + " gmt " + QString::number(gmt)
                            + p.formReturnInfo());
        if(p.isError())
        {
            qInfo()<< "Error at cell i = " << i;
            timer->stop();
        }

        i++;
    }

    for (Point &p : points)
    {
        p.updateAvgs(time);
    }

    //qInfo() << " sum_pressure " << sum_pressure;
}

void Control::advection(Point &p, int index, Vector vector, const double koeff, void advect (Point &p, Point *member, const double koeff))
{
    int tile_x, tile_y;
    points_system.toBilinear(index,tile_x,tile_y); //-- timed
    double vectTravelX = tile_x + vector.x() * koeff;
    double vectTravelY = tile_y + vector.y() * koeff;
    points_system.toSystem(vectTravelX, vectTravelY);

    double fracX = abs(vectTravelX - floor(vectTravelX));
    double fracY = abs(vectTravelY - floor(vectTravelY));

    QVector<int> vectTravelNeibor={
        points_system.toLinear(floor(vectTravelX), floor(vectTravelY)),
        points_system.toLinear(floor(vectTravelX),  ceil(vectTravelY)),
        points_system.toLinear( ceil(vectTravelX),  ceil(vectTravelY)),
        points_system.toLinear( ceil(vectTravelX), floor(vectTravelY))
    };

    double k0 = pow(fmax(1 - Vector(    fracX,     fracY).length(), 0), 2);
    double k1 = pow(fmax(1 - Vector(    fracX, 1 - fracY).length(), 0), 2);
    double k2 = pow(fmax(1 - Vector(1 - fracX, 1 - fracY).length(), 0), 2);
    double k3 = pow(fmax(1 - Vector(1 - fracX,     fracY).length(), 0), 2);
    double sum = k0 + k1 + k2 + k3;

    advect(points[vectTravelNeibor[0]], &p, k0 / sum);
    advect(points[vectTravelNeibor[1]], &p, k1 / sum);
    advect(points[vectTravelNeibor[2]], &p, k2 / sum);
    advect(points[vectTravelNeibor[3]], &p, k3 / sum);
}
void advect_wind(Point &p, Point *member, const double koeff)
{
    advection_point ap = {member, koeff};
    p.radvection_data().push_back(ap);
}
void advect_current(Point &p, Point *member, const double koeff)
{
    advection_point ap = {member, koeff};
    p.rcurrents_data().push_back(ap);
}



QVariantList Control::getData(int id) // int id
{

    QVariantList result;

    switch (id) {
    case 0:
        for (Point &p : points)
            result.append(getColor(id, p.height()));
        return result;
    case 1:
        for (Point &p : points)
            result.append(getColor(id, p.temp_air()));
        return result;
    case 2:
        for (Point &p : points)
            result.append(getColor(id, p.sunAngle()));
        return result;
    case 3:
        for (Point &p : points)
            result.append(getColor(id, p.pressure() + 1010));
        return result;
    case 4:
        for (Point &p : points)
            result.append(getColor(id, p.humidity()));
        return result;
    case 5:
        for (Point &p : points)
            result.append(getColor(4, p.falls() / 10));
        return result;
    case 6:
        for (Point &p : points)
            result.append(getColor(1, p.temp_ground()));
        return result;
    case 7:
        for (Point &p : points)
            result.append(getColor(4, p.cloudness()));
        return result;
    case 8:
        for (Point &p : points)
            result.append(getColor(0, p.height()));
        return result;
    case 9:
        for (Point &p : points)
            result.append(getColor(4, p.humidity() / p.humidity_max() * 100));
        return result;
    case 10:
        for (Point &p : points)
            result.append(Biome_Colors[p.getClimate()]);
        return result;
    case 11:
        for (Point &p : points)
            result.append(Falls_Colors[p.getFallsType()]);
        return result;
    }
    return result;
}

QVariantList Control::getNumbers()
{
    QVariantList result;
    for (Point &p : points)
        result.append(QVariant::fromValue(QVector<double>()<< p.height() << p.temp_air()
                                          << p.sunAngle() << p.pressure() << p.humidity()
                                          << p.falls() << p.temp_ground() << p.cloudness()
                                          << p.current().length() /** 1000*/
                                          << p.humidity() / p.humidity_max() * 100
                                          << p.current().angle() << p.wind().length() << p.wind().angle()));
    /*
     * Pressure, dPressure;
    double Temp_air, dTemp_air;
    double Temp_ground;
    double Humidity, dHumidity, maxHumidity;
    double Cloudness
    */
    return result;
}

void Control::setNumbs(int numb_in_line,int numb_of_lines){

}
void Control::setPointsDensity(const int density_x, const int density_y)
{
    qInfo()<<"=============DEBUG============================";
    //set new points per width and height of simulation
    points_system.set(density_x, density_y);

    //get point dencity on image (pixels)
    const int tile_sx = img.width()/ density_x;
    const int tile_sy = img.height()/ density_y;

    //clear old points (must to be remaded)
    //points.clear();
    //fill it with new points
    points.fill(Point(), points_system.maxLinear());

    int i = 0, x, y;
    for(Point &p : points)
    {
        // get billinear cords of the point
        points_system.toBilinear(i, x, y);
        // get height color from heightmap
        QColor clr;
        if (!img.isNull())
            clr = img.pixelColor(tile_sx/2 + tile_sx*x,
                                  tile_sy/2 + y*tile_sy);

        p.reset();
        QPointF polar_cords = polar_system.translateFrom(points_system,QPointF(x, double(y) + 0.5));
        p.rpos() = QPoint(polar_cords.x(), -polar_cords.y());

        p.rheight() = clr.red();
        p.setNeibours(points[points_system.toLinear(x + 1, y)],
                points[points_system.toLinear(x    , y + 1)],
                points[points_system.toLinear(x - 1, y)],
                points[points_system.toLinear(x    , y - 1)]);

        i++;
    }
    for(Point &p : points)
        p.updateCurrents();
}
//////////////////////////////
QColor Control::getColor(int id, int data)
{
    QColor ret;
    switch (id) {
        case 0:
            if(data<=15)
                ret.setNamedColor("royalblue");
            else if(data<=60)
                ret.setNamedColor("skyblue");
            else if(data<=75)
                ret.setNamedColor("gold");
            else if(data<=120)
                ret.setNamedColor("yellowgreen");
            else if(data<=170)
                ret.setNamedColor("forestgreen");
            else if(data<=235)
                ret.setNamedColor("darkcyan");
            else if(data<=250)
                ret.setNamedColor("darkgrey");
            else
                ret.setNamedColor("ghostwhite");
        break;
    case 1:
        if(data<=-9)
            ret.setNamedColor("blue");
        else if(data<=-6)
            ret.setNamedColor("deepskyblue");
        else if(data<=-3)
            ret.setNamedColor("skyblue");
        else if(data<=0)
            ret.setNamedColor("lightskyblue");
        else if(data<=3)
            ret.setNamedColor("green");
        else if(data<=6)
            ret.setNamedColor("greenyellow");
        else if(data<=9)
            ret.setNamedColor("yellow");
        else if(data<=12)
            ret.setNamedColor("gold");
        else if(data<=16)
            ret.setNamedColor("goldenrod");
        else if(data<=19)
            ret.setNamedColor("purple");
        else if(data<=25)
            ret.setNamedColor("red");
        else if(data<=30)
            ret.setNamedColor("saddlebrown");
        else
            ret.setNamedColor("seashell");
        break;
    case 2:
        if(data<-18)
            ret.setNamedColor("darkblue");
        else if(data<=-12)
            ret.setNamedColor("midnightblue");
        else if(data<=-6)
            ret.setNamedColor("darkslateblue");
        else if(data<=0)
            ret.setNamedColor("royalblue");
        else if(data<=6)
            ret.setNamedColor("cornflowerblue");
        else if(data<=18)
            ret.setNamedColor("skyblue");
        else
            ret.setNamedColor("lightskyblue");
        break;
    case 3:
        if(data<934)
            ret.setNamedColor("lavenderblush");
        else if(data<=944)
            ret.setNamedColor("indigo");
        else if(data<=954)
            ret.setNamedColor("purple");
        else if(data<=964)
            ret.setNamedColor("mediumorchid");
        else if(data<=974)
            ret.setNamedColor("mediumpurple");
        else if(data<=982)
            ret.setNamedColor("skyblue");
        else if(data<=990)
            ret.setNamedColor("mediumaquamarine");
        else if(data<=998)
            ret.setNamedColor("mediumseagreen");
        else if(data<=1006)
            ret.setNamedColor("lightgreen");
        else if(data<=1014)
            ret.setNamedColor("khaki");
        else if(data<=1022)
            ret.setNamedColor("orange");
        else if(data<=1030)
            ret.setNamedColor("darkorange");
        else if(data<=1038)
            ret.setNamedColor("lightcoral");
        else if(data<=1042)
            ret.setNamedColor("palevioletred");
        else if(data<=1052)
            ret.setNamedColor("indianred");
        else if(data<=1062)
            ret.setNamedColor("firebrick");
        else
            ret.setNamedColor("maroon");
        break;
    case 4:
        if(data<=5)
            ret.setNamedColor("khaki");
        else if(data<=15)
            ret.setRgbF(0.6,0.6,0.6);
        else if(data<=30)
            ret.setRgbF(0.7,0.7,0.7);
        else if(data<=45)
            ret.setRgbF(0.8,0.8,0.8);
        else if(data<=60)
            ret.setRgbF(0.9,0.9,0.9);
        else if(data<=75)
            ret.setRgbF(0.95,0.95,0.95);
        else if(data<=90)
            ret.setRgbF(1.,1.,1.);
        else
            ret.setRgbF(0.1,0.2,0.7);
        break;
    }
    return ret;
}
//////////////////////////////
double Control::count_sunAngle(const double lat, const double lon)
{

        double timeAngle=(time.time().hour() * 60+ lon/15 *60+ time.time().minute() - 720) / 4;

        double decl = asin(
                    sin(toRad(maxDeclension)) * sin(
                        toRad(360 / 365.25 * ( time.date().dayOfYear()- 81)))) * 180 / PI;
        double x = cos(timeAngle * PI / 180) * cos(decl * PI / 180);
        double z = sin(decl * PI / 180);
        double zhor = x * cos(lat * PI / 180) + z * sin(lat * PI / 180);

        return (asin(zhor) * (180 / PI));
}
double Control::count_vapourMass(const double temp)
{
    return 136 * pow(2.71828, (18.3036 - 3816.44 / (temp+273-46.13))) * 29 / 8.31 / (temp+273);
}
double Control::billinearInterpolation(const double x, const double y, const double value_0, const double value_1, const double value_2, const double value_3)
{
    return value_0*(1-x)*(1-y)+value_1*x*(1-y)+value_3*(1-x)*y+value_2*x*y;
}


double Control::toRad(const double angle)
{
    return angle * PI/180;
}
QRandomGenerator& Control::getRandomGen()
{
    static QRandomGenerator gen(QTime::currentTime().msec());
    return gen;
}
double Control::getRandom(const double min, const double max)
{
    return getRandomGen().bounded(max - min) + min;
}
int Control::getRandom(const int min, const int max)
{
    return getRandomGen().bounded(max - min) + min;
}

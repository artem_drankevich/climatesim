
QT       += core gui qml quick


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets qml

TARGET = Climate
TEMPLATE = app

###TRY TO IMPLEMENT RUSSIAN
QMAKE_EXTRA_TARGETS += before_build makefilehook

makefilehook.target = $(MAKEFILE)
makefilehook.depends = .beforebuild

PRE_TARGETDEPS += .beforebuild

before_build.target = .beforebuild
before_build.depends = FORCE
before_build.commands = chcp 1251
############


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    cells.cpp \
    control.cpp \
    coordssystem.cpp \
    cyclone.cpp \
    main.cpp \
    point.cpp \
    vector.cpp

HEADERS += \
    cells.h \
    control.h \
    coordssystem.h \
    cyclone.h \
    point.h \
    vector.h


DISTFILES += \
    Display.qml \
    WindDisplay.qml \
    main.qml

RESOURCES += \
    resources.qrc

import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import control 1.0

Window {
    id: window
    width: Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight
    visible: true

    Component.onCompleted: {

    }

    Control {
        id: control
        onTimeChanged: {
            time_text.text = "date " + time.toLocaleString(
                        Qt.locale("en_EN"), "yyyy.MM.dd hh:mm:ss")

            display.updateAll_data()
        }
    }

    //==========Controls=================
    ColumnLayout {
        id: main_column
        y: 8
        spacing: 20
        Text {

            width: 147
            height: 24
            font.pointSize: 17
            id: time_text
            x: 10
        }
        Button {
            id: start_stop
            //x: 24
            //y: time_text.y
            text: "run"
            onClicked: {
                control.toggleTime()
            }
        }
        Button {
            id: discharge
            //x: 159
            //y: 33
            text: qsTr("discharge")
            onClicked: {
                control.discharge()
            }
        }
        Column {
            id: fieldBox
            //x: 0
            //y: 502
            width: 200
            height: 185
            //anchors.right: display.right
            //anchors.left: parent.left

            Text {
                id: element1
                text: qsTr("Field setup")
                font.bold: true
                font.pixelSize: 14
            }

            Slider {
                id: slider_numb_in_line
                from: 10
                value: 50
                to: 60
                stepSize: 1
                onPressedChanged: {
                    if (!pressed)
                        display.setDen_x(value)
                }
                Text {
                    x: 0
                    y: 0
                    text: "Numb_in_line " + slider_numb_in_line.value
                }
            }

            Slider {
                id: slider_fixedWidth
                from: 200
                to: 2500
                value: 1000
                stepSize: 50
                onValueChanged: {
                    display.setFixedWidth(value)
                }
                Text {
                    text: "FixedWidth " + slider_fixedWidth.value
                }
            }
        }
        //=======Check boxes============
        ButtonGroup {
            buttons: column.children
        }
        Column {

            //width: 136
            //height: 162
            id: column
            x: 0
            Text {
                width: 90
                height: 14
                text: qsTr("Choose display mode")
                font.bold: true
                font.pixelSize: 14
            }

            RadioButton {
                height: 30
                checked: true
                text: qsTr("Height mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("height")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Temperature mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("temperature")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Ground T mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("groundt")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Radiation mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("radiation")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Pressure mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("pressure")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Humidity mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("humidity")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Relative Humidity mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("humidity%")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Clouds mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("clouds")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Falls mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("falls")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Falls type mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("fallstp")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Currents mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("currents")
                    }
                }
            }
            RadioButton {
                height: 30
                text: qsTr("Climate mode")
                onCheckedChanged: {
                    if (checked) {
                        display.setMode("climate")
                    }
                }
            }
        }
        Text {
            id: information
            text: qsTr("info")
            font.pixelSize: 15
        }
    }

    //======================
    Column {
        visible: false
        id: cycloneColumn
        x: display.fixed_width + display.x
        //y: 100
        width: 200
        height: 400

        Text {
            text: qsTr("Cyclone setup")
            font.bold: true
            font.pixelSize: 14
        }

        RangeSlider {
            id: slider_cycloneForce
            second.value: 0.75
            first.value: 0.25

            Text {
                text: qsTr("Cyclone force")
                font.pixelSize: 12
            }
        }

        Slider {
            id: slider_cycloneNumb
            value: 0.5

            Text {
                text: qsTr("Cyclone max Numb")
                font.pixelSize: 12
            }
        }

        Slider {
            id: slider
            value: 0.5

            Text {
                text: qsTr("Cyclone max speed")
                font.pixelSize: 12
            }
        }

        RangeSlider {
            id: slider_lifespan
            second.value: 0.75
            first.value: 0.25

            Text {
                text: qsTr("Cyclone lifespan")
                font.pixelSize: 12
            }
        }

        Button {
            id: button1
            text: qsTr("Accept")
        }
    }

    Column {
        visible: false
            id: field_cellEdit
            //x: 1502
            //y: 100
            width: 200
            height: 400
            anchors.top: display.top
            anchors.verticalCenter: cycloneColumn.verticalCenter
            anchors.rightMargin: 6
            //anchors.bottom: cycloneColumn.bottom
            //anchors.right: cycloneColumn.left

            Text {
                id: element7
                text: qsTr("Edit Cell value")
                font.bold: true
                font.pixelSize: 14
            }

            TextInput {
                id: textInput
                width: 80
                height: 20
                text: qsTr("0")
                font.pixelSize: 17
            }
        }
    //}

    Display {
        x: 400
        //y: 100
        //x: main_column.width
        id: display

        //numb_of_lines: 29
        //numb_in_line: 50
        //den_x: 50
        //den_y: 35
        den_x: 60//60//60//50//40//30//15//30
        den_y: 60//29//35//25//19//25//9//19
        fixed_width: 1000
    }
}

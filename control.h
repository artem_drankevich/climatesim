#ifndef CONTROL_H
#define CONTROL_H

#include <QObject>
#include <QImage>
#include <QDebug>
#include <QDateTime>
#include <QTimer>
#include <QVector2D>
#include <QRandomGenerator>
#include <cmath>
#include "coordssystem.h"
#include "point.h"

static const double PI = 3.1415926535;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
class Cyclone;
class Point;
class Vector;
class CoordsSystem;
class Control : public QObject
{
    Q_OBJECT
public:
    Control(QObject *parent = nullptr);
    ~Control();

    Q_INVOKABLE void setNumbs(int numb_in_line, int numb_of_lines);
    Q_INVOKABLE void setPointsDensity(int density_x, int density_y);
    Q_PROPERTY(QDateTime time MEMBER time)

    Q_PROPERTY(QList<QString> info_data MEMBER info_data)
//    Q_PROPERTY(QList<QVariant> color_data MEMBER color_data)

//    Q_PROPERTY(QList<double> wind_angles MEMBER wind_angle)
//    Q_PROPERTY(QList<QVariant> wind_colors MEMBER wind_color)

    Q_INVOKABLE QVariantList getData(int id);
    Q_INVOKABLE QVariantList getNumbers();

    Q_INVOKABLE void toggleTime();
    Q_INVOKABLE void discharge();

    Q_INVOKABLE void setValue(int i, double val, int type);

    static QRandomGenerator& getRandomGen();
    static int getRandom(const int min, const int max);
    static double getRandom(const double min, const double max);
    static double toRad(const double angle);

private:
    //===========================
    QVector<Point> points;
    CoordsSystem points_system;
    CoordsSystem polar_system;
    //=============================

   int timespep = 3600; //(timestep, secs)
   int km_in_tile = 50;//km///100
   int add_delay = 0;//every tick++
   double maxDeclension = 23.45; //23.45

   QList<double> wind_angle;
   QList<QVariant> wind_color;

   QList<QString> info_data;
   QList<QVariant> color_data;

   QVector<Cyclone> cyclones;
   int cyclones_max = 120;//30;//8

   int current_display_id = 0;

   double sum_pressure = 0;

   QImage img;


   void clearAll();
   void tick();
   void updateHeight();
   void updateRadiation();
   void updatePressures();
   void updateTemperatures();

   void advection(Point &p, int index, Vector vector, const double koeff, void advect (Point &p, Point *member, const double koeff));

   double count_sunAngle(const double lat, const double lon);
   double count_vapourMass(const double temp);

//   inline double normalDistribution(const double math_expectation, const double deviation)
//   {
//       double x1,x2,S;
//       do
//       {
//           x1=getRandom(-1.0,1.0);
//           x2=getRandom(-1.0,1.0);
//           S=x1*x1+x2*x2;
//       } while (S>1.0 || S<=0);

//       double Nrm1=math_expectation+x1*sqrt(-2*log(S)/S*deviation*deviation);
//       double next=math_expectation+x2*sqrt(-2*log(S)/S*deviation*deviation);

//       return (Nrm1+next)/2;
//   }
   QColor getColor(const int id, const int data);

   double billinearInterpolation(const double x, const double y, const double value_0, const double value_1, const double value_2, const double value_3);

   QTimer *timer;
   QDateTime time;

   //Cells cells;



signals:
   void timeChanged();

private slots:
    void triggered();

};
#endif // CONTROL_H

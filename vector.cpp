#include "vector.h"
#include "control.h"

Vector::Vector()
{

}
Vector::Vector(const double x,const double y)
{
    X=x;
    Y=y;
}
//Vector(const double x,const double y,const double z);

double Vector::x()
{
    return X;
}
double &Vector::rx()
{
    return X;
}
void Vector::setX(const double x)
{
    X=x;
}

double Vector::y()
{
    return Y;
}
double &Vector::ry()
{
    return Y;
}
void Vector::setY(const double y)
{
    Y=y;
}

//double z();
//double &rz();
//void setZ(const double z);

void Vector::operator+=(const Vector vect)
{
    this->X+=vect.X;
    this->Y+=vect.Y;
}
void Vector::operator-=(const Vector vect)
{
    this->X-=vect.X;
    this->Y-=vect.Y;
}
void Vector::operator*=(const Vector vect)
{
    this->X*=vect.X;
    this->Y*=vect.Y;
}
void Vector::normalize()
{
    double len = this->length();
    if (len < 0.00001)
        return;
    X/= len;
    Y/= len;
}
Vector Vector::normal_clockwise()
{
    return Vector(-Y, X);
}
Vector Vector::normal_anticlockwise()
{
    return Vector(Y, -X);
}
Vector Vector::multiply(const double val)
{
    X*=val;
    Y*=val;
    return *this;
}
double Vector::length()
{
    return sqrt(X*X+Y*Y);
}
double Vector::angle()
{
    double ret;
    if(length()>0)
        ret = acos(X/length()) / PI*180;
    else
        ret = 0;
    if(Y<0)
        ret*=-1;
    return ret;
}

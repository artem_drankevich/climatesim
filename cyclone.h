#ifndef CYCLONE_H
#define CYCLONE_H

#include <QPoint>
#include <cmath>
#include <QDebug>
#include <QRandomGenerator>
#include <QTime>
#include "coordssystem.h"


class Cyclone
{
    enum CycloneType {
        C_CYCLONE, C_ANTICYCLONE
    };

public:
    Cyclone();
    Cyclone(CoordsSystem &cs);

    void spawn(CycloneType type);
    QPointF update(const double sum_pressure, bool &isAlive);
    double getForce();
    double getRadius();
private:
    int lifespan = -1;
    QPointF speed;
    QPointF coords;
    double force;
    double radius;

    CoordsSystem* cs;

};

#endif // CYCLONE_H

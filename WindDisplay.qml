import QtQuick 2.0

Item {
    id: wind
    property color clr
    property double force
    property alias angle: rotator.angle

    Rectangle {
        width: (parent.width) * 0.8
        y: parent.height * 0.45
        x: parent.width * 0.1
        height: parent.height * 0.1
        color: parent.clr
        //Rectangle {
        //    radius: parent.width / 20
        //    y: -parent.width / 20
        //    height: parent.width / 5
        //    width: parent.width / 4
        //    color: parent.color
        //}
    }
    Rectangle {
        //radius: parent.width / 20
        x: parent.width * 0.1
        y: parent.height * 0.4
        height: parent.height * 0.2
        width: parent.width *0.2
        color: parent.clr
    }
    transform: Rotation {
        id: rotator
        origin.x: parent.width / 2
        origin.y: parent.width / 2
    }
}

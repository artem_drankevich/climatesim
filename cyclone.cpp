#include "cyclone.h"
#include "control.h"

Cyclone::Cyclone()
{

}

Cyclone::Cyclone(CoordsSystem &cs)
{
    this->cs = &cs;
}

void Cyclone::spawn(CycloneType type)
{
    //speed.setX(0);
    //speed.setY(0);

    int min_x, min_y, max_x, max_y;
    cs->get(min_x, min_y, max_x, max_y);

    coords.setX(Control::getRandom(min_x, max_x));

    lifespan = Control::getRandom(20,50);

    radius = Control::getRandom(10.,28.) ;

    if(type == C_ANTICYCLONE)
    {
        //anticyclone
        force = Control::getRandom(20.,45.) *0.1*0.2 ; //force=getRandom(20.,35.) *0.1*0.5 ;
        //qInfo()<<"anticyclone lat "<<coords.y()<<" cos "<<-cos(toRad(coords.y()*6)) << " force "<<force;
        speed.setX(Control::getRandom(-10.,10.)/5  );
        speed.setY(Control::getRandom(-10.,10.)/30 ); // / 30
    }
    else
    {
        //cyclone
        speed.setX(Control::getRandom(-10.,10.)/5 *3  );
        speed.setY(Control::getRandom(-10.,10.)/10 );
        force = -Control::getRandom(20.,45.) *0.1*0.5; //force=getRandom(20.,35.) *0.1*0.5 ;
        //qInfo()<<"cyclone lat "<<coords.y()<<" cos "<<-cos(toRad(coords.y()*6)) << " force "<<force;
    }
    do {
        coords.setY(Control::getRandom(min_y, max_y));
    }
    while ( -cos(Control::toRad(coords.y()*6)) > 0 != (type == C_ANTICYCLONE));
    //qInfo()<<"lat "<<coords.y()<<" cos "<<-cos(toRad(coords.y()*6)) << " force "<<force;
}

QPointF Cyclone::update(const double sum_pressure, bool &isAlive)
{
    isAlive = lifespan > 0;

    if (isAlive)
    {
        lifespan--;

        coords += speed;
        cs->toSystem(coords);

        return coords;
    } else {
        if (Control::getRandom(0, 100) > 15 /*&& abs(sum_pressure) > 100*/)
            spawn((sum_pressure > 0 ? C_CYCLONE : C_ANTICYCLONE));
        return QPointF();
    }
}

double Cyclone::getRadius(){
    return radius;
}
double Cyclone::getForce()
{
    return force;//*getRandom(0.9,1.1));
}



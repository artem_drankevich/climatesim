#include "cells.h"

Cells::Cells()
{

}
Cells::Cells(const CoordsSystem sys)
{
    set(sys);
}

void Cells::set(const CoordsSystem sys)
{
    system=sys;

    for(int i=0;i<system.maxLinear();i++)
    {
        heights.push_back(0);
        temperatures.push_back(0);
        radiation.push_back(0);
        pressures.push_back(1010);
        humids.push_back(0);
    }
}

QVector<double> Cells::data(int id)
{
    switch (id) {
    case 0:
        return heights;
    case 1:
        return temperatures;
    case 2:
        return radiation;
    case 3:
        return pressures;
    case 4:
        return humids;
    }
}
QVector<double> &Cells::rdata(int id)
{
    switch (id) {
    case 0:
        return heights;
    case 1:
        return temperatures;
    case 2:
        return radiation;
    case 3:
        return pressures;
    case 4:
        return humids;
    }
}

double &Cells::rtemp(const int i)
{
    return temperatures[i];
}
double Cells::temp(const int i)
{
    return temperatures[i];
}
double &Cells::rrad(const int i)
{
    return radiation[i];
}
double Cells::rad(const int i)
{
    return radiation[i];
}
double &Cells::rhei(const int i)
{
    return heights[i];
}
double Cells::hei(const int i)
{
    return heights[i];
}
double &Cells::rpres(const int i)
{
    return pressures[i];
}
double Cells::pres(const int i)
{
    return pressures[i];
}
double &Cells::rhum(const int i)
{
    return humids[i];
}
double Cells::hum(const int i)
{
    return humids[i];
}

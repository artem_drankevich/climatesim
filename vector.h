#ifndef VECTOR_H
#define VECTOR_H

#include <cmath>

class Vector
{
public:
    Vector();
    Vector(const double x,const double y);
    //Vector(const double x,const double y,const double z);

    double x();
    double &rx();
    void setX(const double x);

    double y();
    double &ry();
    void setY(const double y);

    //double z();
    //double &rz();
    //void setZ(const double z);

    void operator+=(const Vector vect);
    void operator-=(const Vector vect);
    void operator*=(const Vector vect);

    void normalize();

    Vector normal_clockwise();
    Vector normal_anticlockwise();
    Vector multiply(const double val);
    double length();
    double angle();

private:
    double X,Y;
};

#endif // VECTOR_H

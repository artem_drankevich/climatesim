import QtQuick 2.0

Item {
    id: element

    property int den_x
    property int den_y
    property int fixed_width
    property int fixed_height

    property int currentData: 0
    property int currentCell: 0

    property int w

    property var full_data
    property var full_info
    property var full_colors
    property var full_wind_a
    property var full_wind_c

    Component.onCompleted: {
        control.setNumbs(den_x, den_y)
        control.setPointsDensity(den_x, den_y)
        setFixedWidth(fixed_width)

        updateAll_data()
    }

    function updateAll_cords() {
        displayer.model = den_x * den_y

        for (var i = 0; i < displayer.model; i++) {
            displayer.itemAt(i).updateCoords()
        }
    }
    function updateAll_data() {
        full_data = control.getData(currentData) // name swapped!!!
        full_colors = control.getNumbers()
        //console.log("len ", full_data.length, " Pr 0 ", full_data[0].pressure)

        //full_data = control.getData(currentData)
        full_info = control.info_data

//        full_colors = control.color_data

//        full_wind_a = control.wind_angles
//        full_wind_c = control.wind_colors

        for (var i = 0; i < displayer.model; i++) {
            if (currentData != 10 && currentData != 11)
                displayer.itemAt(i).setData(Math.round(full_colors[i][currentData] * 10) / 10, full_data[i]) //Math.round(full_data[i]*10)/10
            else
            {
                displayer.itemAt(i).setData("", full_data[i])
                displayer.itemAt(i).setWind(0, 0, false)
            }
            //displayer.itemAt(i).setData(Math.round(full_data[i]*10)/10, full_colors[i])
            if (currentData == 8 || currentData == 6) {
                displayer.itemAt(i).setWind(full_colors[i][10] + 180,
                                            full_colors[i][8], true)
            } else if (currentData != 2 && currentData != 10)
                displayer.itemAt(i).setWind(full_colors[i][12] + 180,
                                            full_colors[i][11], true)
        }
        info(currentCell)
    }

    function setMode(mode) {
        switch (mode) {
        case "temperature":
            currentData = 1
            break
        case "radiation":
            console.log("rad mode")
            currentData = 2
            break
        case "height":
            console.log("hei mode")
            currentData = 0
            break
        case "pressure":
            console.log("press mode")
            currentData = 3
            break
        case "humidity":
            console.log("humid mode")
            currentData = 4
            break
        case "falls":
            console.log("falls mode")
            currentData = 5
            break
        case "clouds":
            console.log("clouds mode")
            currentData = 7
            break
        case "groundt":
            console.log("groundt mode")
            currentData = 6
            break
        case "currents":
            console.log("currents mode")
            currentData = 8
            break
        case "humidity%":
            console.log("relative humidity mode")
            currentData = 9
            break
        case "climate":
            console.log("climate mode")
            currentData = 10
            break
        case "fallstp":
            console.log("fallstp mode")
            currentData = 11 // 5
            break
        }
        updateAll_data()
    }

    Repeater {
        id: displayer
        delegate: Rectangle {
            color: "dodgerblue"

            //radius: 4
            MouseArea {
                hoverEnabled: true
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onEntered: {
                    currentCell = index
                    info(index)
                }
                onClicked: {
                    if (mouse.button == Qt.RightButton)
                        control.setValue(index, -20, currentData)
                    else
                        control.setValue(index, 20, currentData)
                }
            }
            WindDisplay {
                id: winddisp
            }

            Text {
                id: display_info
            }
            function setWind(angle, clr, isOn) {
                if (isOn) {
                    winddisp.visible = true

                    // decode clr
                    var color;
                        if(clr<=-9)
                            color = "blue"
                        else if(clr<=-6)
                            color = "deepskyblue"
                        else if(clr<=-3)
                            color = "skyblue"
                        else if(clr<=0)
                            color = "lightskyblue"
                        else if(clr<=3)
                            color = "green"
                        else if(clr<=6)
                            color = "greenyellow"
                        else if(clr<=9)
                            color = "yellow"
                        else if(clr<=12)
                            color = "gold"
                        else if(clr<=16)
                            color = "goldenrod"
                        else if(clr<=19)
                            color = "purple"
                        else if(clr<=25)
                            color = "red"
                        else if(clr<=30)
                            color = "saddlebrown"
                        else
                            color = "seashell"

                    winddisp.clr = color
                    winddisp.angle = angle
                } else
                    winddisp.visible = false
            }
            function setData(data, clr) {
                display_info.text = data
                color = clr
            }
            function updateCoords() {
                winddisp.width = w
                winddisp.height = w
                width = w
                height = w
                x = index % den_x * w
                y = (index - index % den_x) / den_x * w
            }
        }
    }

    function info(i) {
        information.text = "cell " + i + "\n" + full_info[i]
    }

    function setFixedWidth(fixed_w) {
        fixed_width = fixed_w
        w = Math.ceil(fixed_width / den_x)
        fixed_height = w * den_y
        updateAll_cords()
    }

    function setDen_x(new_numb_in_line) {
        den_x = new_numb_in_line
        w = Math.ceil(fixed_width / den_x)
        den_y = fixed_height / w

        control.setNumbs(den_x, den_y)

        updateAll_cords()
        updateAll_data()
    }
}
